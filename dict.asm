%include "lib.inc"
%define keysize 8
global find_word
section .text

; find_word traverses the linked dictionary and returns KEY (not value) if found, else zero
; rdi - pointer to sought for word 
; rsi - pointer to first element in dictionary

find_word: 
    .loop:
        test rsi, rsi       ; check if end of list has been reached
        jz .not_found       

        push rdi
        push rsi
        add rsi, keysize    ; now rsi points at key
                            ; rdi is the word we are looking for
        call string_equals  ; returns 1 if equal, else 0
        pop rsi
        pop rdi

        test rax, rax       
        jnz .found          ; if rax !=0, word was found

        mov rsi, [rsi]      ; else - get the address of the next element
        jmp .loop

    .found:
        mov rax, rsi        ; ret address of key
        ret

    .not_found:
        xor rax, rax        ; ret 0
        ret

        