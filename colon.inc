


%define last_label 0x0
%macro colon 2
  %ifnstr %1
    %error "first arg must be a string!"
  %else
    %ifnid %2
      %error "second arg must be a label!"
    %else
      %2: dq last_label
      db %1, 0
      %define last_label %2

    %endif
  %endif
%endmacro