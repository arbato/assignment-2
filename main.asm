%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define size_buff 256

global _start

section .rodata
input_error: db "Invalid input", `\n`, 0
not_found: db "Key not found in dictionary", `\n`, 0

section .bss
buff: resb size_buff					; create a buffer in uninitialized data sector

section .text

_start:		
	mov rdi, buff 				; put the address of the buffer in rdi
	mov rsi, size_buff			; put buffer size in rsi
	call read_word 				; if word was read incorrectly, rax = 0, else rax has addres and rdx = word length

	test rax, rax 
	jz .input_error 			; check if rax is zero

	mov rdi, buff 				; mov word address to rdi
	mov rsi, last_label			; mov the last dict key to rsi
	call find_word				; returns zero or address of key in dictionary

	test rax, rax
	jz .not_found 		

	mov rdi, rax       			; put addres of key in rdi
	add rdi, 8         			; add 8 to obtain key string
	call string_length 			; get string length
	add rdi, rax       			; add beginning of key string to to length of key string +1 for null term. the result is a pointer to the value string. 
	inc rdi            
	jmp .end

	.input_error:
		mov rdi, input_error 	; get input error arg
		jmp .enderr

	.not_found:
		mov rdi, not_found 		; get find error arg

		
	.enderr:
		call print_error 		; outputs to stderr
		xor rdi, rdi			; returns zero
		call exit

	.end:
		call print_string 		; prints value and exits
		call print_newline		
		call exit
